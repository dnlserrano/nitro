require "csv"

n = 0
xs = []
fs = []
hs = []
labels = []
outcomes = []

CSV.foreach("data/invertebrates.csv").with_index do |row, i|
  if i == 0
    hs = row[1..row.length - 2].map(&:to_sym)
    next
  end

  n += 1

  x = row[1..row.length - 1].map(&:to_i)
  xs << x

  outcomes << row[row.length - 1].to_i
  labels << row[0]
end

all = xs.count

Sample = Struct.new(*hs, :outcome)
xs.map! { |x| Sample.new(*x) }

def normalize(ws)
  sum = ws.reduce(:+)
  ws.map { |w| w / sum }
end

def adaboost_iterate(xs, hs, ws, fs)
  errors = {}

  hs.length.times do |i|
    h = hs[i]
    errors[h] = xs.reduce(0) { |sum, x|
      y = x[:outcome]
      sum + ((x[h] != y ? 1 : 0) * ws[i])
    }
  end

  min_e = errors.min_by { |k, v| v }
  arg_min = min_e[0]
  e = min_e[1]

  a = 0.5 * Math.log((1 - e) / e)

  fs << {alpha: a, variable: arg_min}

  new_ws = []
  ws.each_with_index do |w, i|
    new_ws << w * Math.exp(- xs[i][:outcome] * a * xs[i][arg_min])
  end

  new_ws = normalize(new_ws)

  remaining_features = (hs - [arg_min])
  [xs, remaining_features, new_ws, fs]
end

ws = [1.0 / n] * n

3.times do |i|
  xs, hs, ws, fs = adaboost_iterate(xs, hs, ws, fs)
end

def output(fs, x)
  output = fs.reduce(0) do |sum, f|
    sum + (f[:alpha] * x[f[:variable]])
  end

  output > 0 ? 1 : -1
end

true_positives = 0

xs.each_with_index do |x, i|
  true_positive = (outcomes[i] == output(fs, x))
  true_positives += 1 if true_positive
end

puts fs.map { |f| "#{f[:alpha]} * #{f[:variable]}" }.join(" + ")
puts "#{(true_positives.to_f / all * 100).round(2)} %"
